package piskiewicz.bartosz.common.repository;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(repositoryBaseClass = QueryRepositoryImpl.class, basePackages = "piskiewicz.bartosz.common")
public class QueryRepositoryConfiguration {
}
